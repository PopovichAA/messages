let redis = require( "redis" );
// клиент для публикации и подписи
let client = redis.createClient();
// клиент для работы с ошибками
let errorClient = redis.createClient();

// дата последнего сообщения
let lastMessageDate = null;

// время повторения проверки наличия публикатора
const CHECK_PUBLISHER_TIME = 5000;
// время повторения отправки сообщения
const PUBLISH_TIME = 500;

// интервал с проверкой публикатора
let checkPublisherInterval = setInterval( checkPublisher, CHECK_PUBLISHER_TIME );

/**
 * Проверка наличия публикатора
 */
function checkPublisher() {
	// если не получали сообщения или это было давно, 
	// то отписываемся от канала и начинаем публиковать,
	// также удаляем интервал с проверкой публикатора
	if( !lastMessageDate || ( ( Date.now() - lastMessageDate ) >= CHECK_PUBLISHER_TIME ) ) {
		console.log( "Start publish" );
		client.unsubscribe( "main channel" );
		startPublish();
		clearInterval( checkPublisherInterval );
	}
}

// подпись на канал
client.subscribe( "main channel" );

// обработка получения сообщения 
client.on( "message", function ( channel, message ) {
	// обновляем дату последнего сообщения
	lastMessageDate = Date.now();
    console.log( `${channel} => ${message}`);    
} );

/**
 * Запуск публикации
 */
function startPublish() {
	setInterval( () => {
		// случайное сообщение 
		let number = Math.round( Math.random() * 1000 );
		let msg = `Message #${number}`;	
				
		// считаем что это 5% ошибка, сохраняем ее в бд
		if( number <= 50 ) {
			errorClient.rpush(["messageErrors", msg], function(err, reply) {
			    console.log( `Save error: ${msg}`); 
			} );		
		}
		// публикуем сообщение в обычном режиме
		else {
			client.publish( "main channel", msg );
		}		
	}, PUBLISH_TIME );
}

// если приложение запущено с флагом getErrors,
// то отображаем ошибки и удаляем их
if( process.argv.includes( "getErrors" ) ) {
	errorClient.lrange( "messageErrors", 0, -1, function( err, reply ) {
	    console.log( `Errors : ${reply.length}` ); 

		// если есть ошибки 
	    if( reply.length > 0 ) {
	    	// детальная информация
	    	console.log( reply ); 

	    	// удаляем
			errorClient.del( "messageErrors", function( err, reply ) {
			    console.log( "Messages have been removed" ); 
			    process.exit();
			} );			
	    } else {
	    	process.exit();
	    }
	} );
}